#API для клиента

#### GET /penza/users
```
{
  "data": {
    "userIds": [1, 2, 3, 4, 5]
  },
  "status": 200
}
```


#### POST /penza/users
Добавить пользователя в комнату

Request 
```
{
  userId: 1
}
```


#### POST /penza/user/:toUserId/comments
Добавить комментарий к пользователю в комнате
Request 
```
{
  text: 'Всего',
  userId: 1, // свой userId, пока так, не думай о безопасности и накрутках
}
```

#### GET /penza/users/:userId/comments
список пожеланий на пользователтя

```json
{
  "data": {
    "comments": [
      {
        "_id": "586019b72a136b4b66e70aca",
        "text": "Всего хорошего в новом году!",
        "userId": 1,
        "toUserId": 7
      },
      {
        "_id": "58601bc066f4464bf8e9f92c",
        "text": "Счастья и любви",
        "userId": 2,
        "toUserId": 7
      }
    ]
  },
  "status": 200
}
```

#### GET /penza/comments
Список всех пожеланий в комнате

```json
{
  "data": {
    "comments": [
      {
        "_id": "58600e6ea2a47d485c7513bf",
        "text": "Всего хорошего в новом году!",
        "userId": 1,
        "toUserId": 2
      },
      {
        "_id": "58600e83be289448883a4717",
        "text": "Всего хорошего в новом году!",
        "userId": 1,
        "toUserId": 3
      },
      {
        "_id": "58601bc066f4464bf8e9f92c",
        "text": "Счастья и любви",
        "userId": 2,
        "toUserId": 7
      }
    ]
  },
  "status": 200
}
```

#### POST /comments/:commentId/like
Поставить лайк комменту
Request 
```
{
    "userId": 1,
}
```

#### DELETE /comments/:commentId/like
Удалить лайк комменту
```
{
    "userId": 1,
}
```