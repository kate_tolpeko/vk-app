import {Component, OnInit} from '@angular/core';
import {VkApi} from "../../services/vkApi.service";
import {ServerApi} from "../../services/serverApi.service";
import {Params, ActivatedRoute, Router} from "@angular/router";
import {User} from "../../interfaces/user.interface";
import {Comment} from "../comment.interface";
import {City} from "../../interfaces/city.interface";


@Component({
    moduleId: module.id,
    selector: 'user',
    templateUrl: 'user.component.html',
    providers: [VkApi, ServerApi]
})


export class UserComponent extends OnInit {

    constructor(private vkApi: VkApi, private serverApi: ServerApi, private route: ActivatedRoute, private router: Router) {
    }

    private userInfo: User;
    private commentsOfUser: Comment[];
    private currentUserId: number;
    private isLiked: boolean;
    private likeImg = "../../../img/like.png";
    private noLikeImg = "../../../img/no-like.png";
    private city: string;
    private errorMessage: string;

    ngOnInit(): void {
        this.route.params
            .map((params: Params) => [params['city'], params['id']])
            .subscribe(([city,id]) => {
                this.serverApi.getCities()
                    .then((cities)=> {
                        if (!this.checkCity(city, cities)) {
                            throw('Неверный город')
                        }
                        this.city = city;
                        return this.vkApi.getCurrentUserStatus();
                    })
                    .then((usId)=> {
                        this.currentUserId = parseInt(usId);
                        return this.vkApi.getSingleUserInfo(id)
                    })
                    .then((userInfo)=> {
                        this.userInfo = userInfo;
                        return this.serverApi.getUserComments(userInfo.uid, this.city);
                    })
                    .then((data)=> {
                        this.commentsOfUser = data.comments;
                    })
                    .catch((error)=> {
                        this.router.navigate(['']);
                    })
            })
    }

    addComment(textField: any) {
        if (this.userInfo.uid === this.currentUserId) {
            this.errorMessage = "Нельзя оставить пожелание самому себе";
        } else {
            this.errorMessage = "";
            this.serverApi.addComment(textField.value, this.userInfo.uid, this.currentUserId, this.city)
                .then(()=> {
                    return this.serverApi.getUserComments(this.userInfo.uid, this.city);
                })
                .then((data)=> {
                    this.commentsOfUser = data.comments;
                })
                .catch((error)=> {
                    this.errorMessage = "Вы уже отправили пожелание этому человеку"
                })
        }
    }


    like(commentId: number) {
        for (let i = 0; i < this.commentsOfUser.length; i++) {
            if (this.commentsOfUser[i]._id == commentId) {
                if (this.commentsOfUser[i].likes.indexOf(parseInt(this.currentUserId)) === -1) {
                    this.serverApi.addLike(this.commentsOfUser[i]._id, this.currentUserId)
                        .then(()=> {
                            return this.serverApi.getUserComments(this.userInfo.uid, this.city);
                        })
                        .then((data)=> {
                            this.isLiked = true;
                            this.commentsOfUser = data.comments;
                        });
                }
                else {
                    this.serverApi.deleteLike(this.commentsOfUser[i]._id, this.currentUserId)
                        .then(()=> {
                            return this.serverApi.getUserComments(this.userInfo.uid, this.city);
                        })
                        .then((data)=> {
                            this.isLiked = false;
                            this.commentsOfUser = data.comments;
                        });
                }
            }
        }
    }

    checkCity(checkCity: string, allCities: City[]): boolean {
        function isCorrectCity(city) {
            return city.url == checkCity;
        }

        return allCities.some(isCorrectCity);
    }
}

