"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var vkApi_service_1 = require("../../services/vkApi.service");
var serverApi_service_1 = require("../../services/serverApi.service");
var router_1 = require("@angular/router");
var UserComponent = (function (_super) {
    __extends(UserComponent, _super);
    function UserComponent(vkApi, serverApi, route, router) {
        this.vkApi = vkApi;
        this.serverApi = serverApi;
        this.route = route;
        this.router = router;
        this.likeImg = "../../../img/like.png";
        this.noLikeImg = "../../../img/no-like.png";
    }
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .map(function (params) { return [params['city'], params['id']]; })
            .subscribe(function (_a) {
            var city = _a[0], id = _a[1];
            _this.serverApi.getCities()
                .then(function (cities) {
                if (!_this.checkCity(city, cities)) {
                    throw ('Неверный город');
                }
                _this.city = city;
                return _this.vkApi.getCurrentUserStatus();
            })
                .then(function (usId) {
                _this.currentUserId = parseInt(usId);
                return _this.vkApi.getSingleUserInfo(id);
            })
                .then(function (userInfo) {
                _this.userInfo = userInfo;
                return _this.serverApi.getUserComments(userInfo.uid, _this.city);
            })
                .then(function (data) {
                _this.commentsOfUser = data.comments;
            })
                .catch(function (error) {
                _this.router.navigate(['']);
            });
        });
    };
    UserComponent.prototype.addComment = function (textField) {
        var _this = this;
        if (this.userInfo.uid === this.currentUserId) {
            this.errorMessage = "Нельзя оставить пожелание самому себе";
        }
        else {
            this.errorMessage = "";
            this.serverApi.addComment(textField.value, this.userInfo.uid, this.currentUserId, this.city)
                .then(function () {
                return _this.serverApi.getUserComments(_this.userInfo.uid, _this.city);
            })
                .then(function (data) {
                _this.commentsOfUser = data.comments;
            })
                .catch(function (error) {
                _this.errorMessage = "Вы уже отправили пожелание этому человеку";
            });
        }
    };
    UserComponent.prototype.like = function (commentId) {
        var _this = this;
        for (var i = 0; i < this.commentsOfUser.length; i++) {
            if (this.commentsOfUser[i]._id == commentId) {
                if (this.commentsOfUser[i].likes.indexOf(parseInt(this.currentUserId)) === -1) {
                    this.serverApi.addLike(this.commentsOfUser[i]._id, this.currentUserId)
                        .then(function () {
                        return _this.serverApi.getUserComments(_this.userInfo.uid, _this.city);
                    })
                        .then(function (data) {
                        _this.isLiked = true;
                        _this.commentsOfUser = data.comments;
                    });
                }
                else {
                    this.serverApi.deleteLike(this.commentsOfUser[i]._id, this.currentUserId)
                        .then(function () {
                        return _this.serverApi.getUserComments(_this.userInfo.uid, _this.city);
                    })
                        .then(function (data) {
                        _this.isLiked = false;
                        _this.commentsOfUser = data.comments;
                    });
                }
            }
        }
    };
    UserComponent.prototype.checkCity = function (checkCity, allCities) {
        function isCorrectCity(city) {
            return city.url == checkCity;
        }
        return allCities.some(isCorrectCity);
    };
    UserComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'user',
            templateUrl: 'user.component.html',
            providers: [vkApi_service_1.VkApi, serverApi_service_1.ServerApi]
        }), 
        __metadata('design:paramtypes', [vkApi_service_1.VkApi, serverApi_service_1.ServerApi, router_1.ActivatedRoute, router_1.Router])
    ], UserComponent);
    return UserComponent;
}(core_1.OnInit));
exports.UserComponent = UserComponent;
//# sourceMappingURL=user.component.js.map