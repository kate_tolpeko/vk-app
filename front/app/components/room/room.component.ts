import {Component, OnInit} from '@angular/core';
import {VkApi} from "../../services/vkApi.service";
import {ServerApi} from "../../services/serverApi.service";
import {User} from "../../interfaces/user.interface";
import {Params, Router, ActivatedRoute} from "@angular/router";
import {City} from "../../interfaces/city.interface";


@Component({
    moduleId: module.id,
    selector: 'room',
    templateUrl: 'room.component.html',
    providers: [VkApi, ServerApi]
})

export class RoomComponent extends OnInit {

    constructor(private vkApi: VkApi, private serverApi: ServerApi, private router: Router, private route: ActivatedRoute) {
    }

    private cityUsers: User[];
    private city: string;

    ngOnInit(): void {
        this.route.params
            .map((params: Params) => params['city'])
            .subscribe((city) => {
                this.serverApi.getCities()
                    .then((cities)=> {
                        if (!this.checkCity(city, cities)) {
                            throw('Неверный город')
                        }
                        this.city = city;
                        return this.vkApi.getCurrentUserStatus();
                    })
                    .then((id) => {
                        return this.serverApi.addUser(id, this.city);
                    })
                    .then((response) => {
                        return this.vkApi.getUsersInfo(response.userIds);
                    })
                    .then((usersInfo)=> {
                        this.cityUsers = usersInfo;
                    })
                    .catch((error)=> {
                        this.router.navigate(['']);
                    })
            })
    }

    checkCity(checkCity: string, allCities: City[]): boolean {
        function isCorrectCity(city) {
            return city.url == checkCity;
        }

        return allCities.some(isCorrectCity);
    }
}

