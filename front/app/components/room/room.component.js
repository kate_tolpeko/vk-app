"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var vkApi_service_1 = require("../../services/vkApi.service");
var serverApi_service_1 = require("../../services/serverApi.service");
var router_1 = require("@angular/router");
var RoomComponent = (function (_super) {
    __extends(RoomComponent, _super);
    function RoomComponent(vkApi, serverApi, router, route) {
        this.vkApi = vkApi;
        this.serverApi = serverApi;
        this.router = router;
        this.route = route;
    }
    RoomComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .map(function (params) { return params['city']; })
            .subscribe(function (city) {
            _this.serverApi.getCities()
                .then(function (cities) {
                if (!_this.checkCity(city, cities)) {
                    throw ('Неверный город');
                }
                _this.city = city;
                return _this.vkApi.getCurrentUserStatus();
            })
                .then(function (id) {
                return _this.serverApi.addUser(id, _this.city);
            })
                .then(function (response) {
                return _this.vkApi.getUsersInfo(response.userIds);
            })
                .then(function (usersInfo) {
                _this.cityUsers = usersInfo;
            })
                .catch(function (error) {
                _this.router.navigate(['']);
            });
        });
    };
    RoomComponent.prototype.checkCity = function (checkCity, allCities) {
        function isCorrectCity(city) {
            return city.url == checkCity;
        }
        return allCities.some(isCorrectCity);
    };
    RoomComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'room',
            templateUrl: 'room.component.html',
            providers: [vkApi_service_1.VkApi, serverApi_service_1.ServerApi]
        }), 
        __metadata('design:paramtypes', [vkApi_service_1.VkApi, serverApi_service_1.ServerApi, router_1.Router, router_1.ActivatedRoute])
    ], RoomComponent);
    return RoomComponent;
}(core_1.OnInit));
exports.RoomComponent = RoomComponent;
//# sourceMappingURL=room.component.js.map