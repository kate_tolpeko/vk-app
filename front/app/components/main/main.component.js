"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var vkApi_service_1 = require("../../services/vkApi.service");
var serverApi_service_1 = require("../../services/serverApi.service");
var MainComponent = (function () {
    function MainComponent(serverApi) {
        this.serverApi = serverApi;
    }
    MainComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.serverApi.getCities()
            .then(function (res) {
            _this.cities = res;
        });
    };
    MainComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'main',
            templateUrl: 'main.component.html',
            providers: [vkApi_service_1.VkApi, serverApi_service_1.ServerApi]
        }), 
        __metadata('design:paramtypes', [serverApi_service_1.ServerApi])
    ], MainComponent);
    return MainComponent;
}());
exports.MainComponent = MainComponent;
//# sourceMappingURL=main.component.js.map