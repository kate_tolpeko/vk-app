import {Component} from '@angular/core';
import {VkApi} from "../../services/vkApi.service";
import {ServerApi} from "../../services/serverApi.service";
import {City} from "../../interfaces/city.interface";

@Component({
    moduleId: module.id,
    selector: 'main',
    templateUrl: 'main.component.html',
    providers: [VkApi, ServerApi]
})

export class MainComponent {

    constructor(private serverApi: ServerApi) {
    }

    private cities: City[];

    ngOnInit(): void {
        this.serverApi.getCities()
            .then((res)=> {
                this.cities = res;
            })
    }
}


