import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UserComponent} from "./components/user/user.component";
import {RoomComponent} from "./components/room/room.component";
import {MainComponent} from "./components/main/main.component";

const routes: Routes = [
    {
        path: '',
        component: MainComponent
    },
    {
        path: ':city',
        component: RoomComponent
    },
    {
        path: ':city/user/:id',
        component: UserComponent
    }
];
@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
