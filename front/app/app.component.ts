import {Component, OnInit} from '@angular/core';
import {VkApi} from "./services/vkApi.service";
import {ServerApi} from "./services/serverApi.service";



@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html',
    providers:[VkApi,ServerApi],
})

export class AppComponent extends OnInit{

    constructor(private vkApi: VkApi) {}
    ngOnInit(): void {
        this.vkApi.getCurrentUserStatus()
            .then((id) => {
                console.log('авторизоваляся как '+id);
            })
    }
}

