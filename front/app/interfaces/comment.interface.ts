export interface Comment {
    _id: number,
    text: string,
    userId: number,
    toUserId: number,
    likes: number[],
    likeCount: number
}
