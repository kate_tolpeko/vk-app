/**
 * Created by katetolpeko on 28.12.16.
 */
export interface City {
    name: string,
    url: string
}
