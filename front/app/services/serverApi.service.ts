import {Injectable}    from '@angular/core';
import 'rxjs/add/operator/map';
import {Http} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import {Comment} from "../interfaces/comment.interface";


@Injectable()
export class ServerApi {

    constructor(private http: Http) {
    }

    private baseUrl = "http://andersen.alexpts.ru:3000";
    private cities = [
        {name: "Andersen", url: "andersen"},
        {name: "Пенза", url: "penza"},
        {name: "Полоцк", url: "polotsk"},
        {name: "Витебск", url: "vitebsk"},
        {name: "Минск", url: "minsk"},
        {name: "Черкассы", url: "cherkassy"},
        {name: "Харьков", url: "harcov"},
        {name: "Санкт-Петербург", url: "spb"},
        {name: "Одесса", url: "odessa"},
        {name: "Москва", url: "moscow"},
        {name: "Чернигов", url: "chernigov"},
        {name: "Вильнюс", url: "vilnyus"},
        {name: "Париж", url: "paris"}
    ];

    getCities() {
        // const url = `//localhost:3001/penza/users`;
        // return this.http.get(url)
        //     .toPromise()
        //     .then(res => res.json().data);
        return Promise.resolve(this.cities);
    }

    addUser(userId: string, city: string) {
        const url = this.baseUrl + `/${city}/users`;
        return this.http.post(url, {userId: userId}, {headers: {'Content-Type': 'application/json'}})
            .toPromise()
            .then(function () {
                return this.http.get(url)
                    .toPromise()
                    .then(res => res.json().data as number[]);
            }.bind(this));
    }

    getUserComments(userId: number, city: string) {
        const url = this.baseUrl + `/${city}/users/${userId}/comments`;
        return this.http.get(url)
            .toPromise()
            .then(res => res.json().data as Comment[]);
    }


    addComment(text: string, toUserId: number, fromUserId:string, city: string) {
        const url = this.baseUrl + `/${city}/users/${toUserId}/comments`;
        return this.http.post(url, {userId: fromUserId, text: text}, {headers: {'Content-Type': 'application/json'}})
            .toPromise()
    }

    deleteLike(commentId: number, fromUserId: number) {
        const url = this.baseUrl + `/comments/${commentId}/like`;
        return this.http.delete(url, {headers: {'Content-Type': 'application/json'}, body: {userId: fromUserId}})
            .toPromise();
    }

    addLike(commentId: number, fromUserId: number) {
        const url = this.baseUrl + `/comments/${commentId}/like`;
        return this.http.post(url, {userId: fromUserId}, {headers: {'Content-Type': 'application/json'}})
            .toPromise();
    }
}

