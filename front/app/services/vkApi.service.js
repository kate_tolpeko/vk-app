"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/map');
var VkApi = (function () {
    function VkApi() {
        this.appId = 5787636;
    }
    VkApi.prototype.getUsersInfo = function (userIds) {
        var stringOfIds = userIds.join(', ');
        return new Promise(function (resolve, reject) {
            VK.Api.call('users.get', { user_ids: stringOfIds, fields: 'photo_200', https: 1 }, (function (response) {
                resolve(response.response);
            }));
        });
    };
    VkApi.prototype.getSingleUserInfo = function (userId) {
        return new Promise(function (resolve, reject) {
            VK.Api.call('users.get', { user_ids: userId, fields: 'photo_200', https: 1 }, (function (response) {
                resolve(response.response[0]);
            }));
        });
    };
    VkApi.prototype.getCurrentUserStatus = function () {
        VK.init({
            apiId: this.appId
        });
        return new Promise(function (resolve, reject) {
            VK.Auth.getLoginStatus((function (response) {
                if (response.status == 'unknown' || response.status == 'not_authorized') {
                    VK.Auth.login(function (response) {
                        resolve(response.session.mid);
                    });
                }
                else {
                    resolve(response.session.mid);
                }
            }));
        });
    };
    VkApi = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], VkApi);
    return VkApi;
}());
exports.VkApi = VkApi;
//# sourceMappingURL=vkApi.service.js.map