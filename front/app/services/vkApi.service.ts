import {Injectable}    from '@angular/core';
import 'rxjs/add/operator/map';


@Injectable()
export class VkApi {

    private appId = 5787636;

    getUsersInfo(userIds: number[]) {
        let stringOfIds = userIds.join(', ');
        return new Promise(function (resolve, reject) {
            VK.Api.call('users.get', {user_ids: stringOfIds, fields: 'photo_200', https: 1}, (response => {
                resolve(response.response);
            }));
        });
    }

    getSingleUserInfo(userId: number) {
        return new Promise(function (resolve, reject) {
            VK.Api.call('users.get', {user_ids: userId, fields: 'photo_200', https: 1}, (response => {
                resolve(response.response[0]);
            }));
        });
    }

    getCurrentUserStatus() {
        VK.init({
            apiId: this.appId
        });
        return new Promise(function (resolve, reject) {
            VK.Auth.getLoginStatus((response => {
                if (response.status == 'unknown' || response.status == 'not_authorized') {
                    VK.Auth.login(response => {
                        resolve(response.session.mid);
                    });
                }
                else {
                    resolve(response.session.mid);
                }
            }));
        });
    }
}

