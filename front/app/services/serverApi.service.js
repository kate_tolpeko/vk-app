"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/map');
var http_1 = require("@angular/http");
require('rxjs/add/operator/toPromise');
var ServerApi = (function () {
    function ServerApi(http) {
        this.http = http;
        this.baseUrl = "http://andersen.alexpts.ru:3000";
        this.cities = [
            { name: "Andersen", url: "andersen" },
            { name: "Пенза", url: "penza" },
            { name: "Полоцк", url: "polotsk" },
            { name: "Витебск", url: "vitebsk" },
            { name: "Минск", url: "minsk" },
            { name: "Черкассы", url: "cherkassy" },
            { name: "Харьков", url: "harcov" },
            { name: "Санкт-Петербург", url: "spb" },
            { name: "Одесса", url: "odessa" },
            { name: "Москва", url: "moscow" },
            { name: "Чернигов", url: "chernigov" },
            { name: "Вильнюс", url: "vilnyus" },
            { name: "Париж", url: "paris" }
        ];
    }
    ServerApi.prototype.getCities = function () {
        // const url = `//localhost:3001/penza/users`;
        // return this.http.get(url)
        //     .toPromise()
        //     .then(res => res.json().data);
        return Promise.resolve(this.cities);
    };
    ServerApi.prototype.addUser = function (userId, city) {
        var url = this.baseUrl + ("/" + city + "/users");
        return this.http.post(url, { userId: userId }, { headers: { 'Content-Type': 'application/json' } })
            .toPromise()
            .then(function () {
            return this.http.get(url)
                .toPromise()
                .then(function (res) { return res.json().data; });
        }.bind(this));
    };
    ServerApi.prototype.getUserComments = function (userId, city) {
        var url = this.baseUrl + ("/" + city + "/users/" + userId + "/comments");
        return this.http.get(url)
            .toPromise()
            .then(function (res) { return res.json().data; });
    };
    ServerApi.prototype.addComment = function (text, toUserId, fromUserId, city) {
        var url = this.baseUrl + ("/" + city + "/users/" + toUserId + "/comments");
        return this.http.post(url, { userId: fromUserId, text: text }, { headers: { 'Content-Type': 'application/json' } })
            .toPromise();
    };
    ServerApi.prototype.deleteLike = function (commentId, fromUserId) {
        var url = this.baseUrl + ("/comments/" + commentId + "/like");
        return this.http.delete(url, { headers: { 'Content-Type': 'application/json' }, body: { userId: fromUserId } })
            .toPromise();
    };
    ServerApi.prototype.addLike = function (commentId, fromUserId) {
        var url = this.baseUrl + ("/comments/" + commentId + "/like");
        return this.http.post(url, { userId: fromUserId }, { headers: { 'Content-Type': 'application/json' } })
            .toPromise();
    };
    ServerApi = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ServerApi);
    return ServerApi;
}());
exports.ServerApi = ServerApi;
//# sourceMappingURL=serverApi.service.js.map