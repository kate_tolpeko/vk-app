import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent}  from './app.component';
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {VkApi} from "./services/vkApi.service";
import {AppRoutingModule} from "./app-routing.module";
import {ServerApi} from "./services/serverApi.service";

import {UserComponent} from "./components/user/user.component";
import {RoomComponent} from "./components/room/room.component";
import {MainComponent} from "./components/main/main.component";


@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpModule
    ],

    declarations: [
        AppComponent,
        UserComponent,
        RoomComponent,
        MainComponent
    ],
    providers: [
        VkApi,
        ServerApi
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
