"use strict";
const router = require('koa-router')();
const mongodb = require('mongodb');

// Получить пользователей в комнате
router.get('/:city/users', async (ctx) => {
    const usersRepo = ctx.mongoDb.collection('users');
    const docs = await usersRepo.find({city: ctx.params.city}, {userId: 1, _id: 0}).toArray();

    ctx.body = {
        'data': {
            'userIds': docs.map((item) => item.userId)
        }
    };
    ctx.type = 'application/json; charset=utf-8';
});

// Добавить комментарий для пользователя в комнате
router.post('/:city/users/:toUserId/comments', async (ctx) => {
    const request = ctx.request.body;
    const toUserId = Number(ctx.params.toUserId);
    const userId = Number(request.userId);

    if (!request.text || request.text.length < 3) {
        ctx.throw(400, 'Bad param text');
    }

    if (!userId) {
        ctx.throw(400, 'Bad param userId');
    }

    if (toUserId === userId) {
        ctx.throw(400, 'Bad param userId');
    }

    const usersRepo = ctx.mongoDb.collection('users');
    const countUsers = await usersRepo.count({
        userId: {'$in': [toUserId, userId]}
    });

    if (countUsers < 2) {
        ctx.throw(400, 'Bad param userId or toUserId');
    }

    const commentsRepo = ctx.mongoDb.collection('comments');
    await commentsRepo.insertOne({
        city: ctx.params.city,
        text: request.text,
        userId: userId,
        toUserId: toUserId,
        likes: [],
    });

    ctx.type = 'application/json; charset=utf-8';
    ctx.body = {};
    ctx.status = 201;
});

// Получить комментарии для пользователя в комнате
router.get('/:city/users/:userId/comments', async (ctx) => {
    const commentsRepo = ctx.mongoDb.collection('comments');
    let docs = await commentsRepo.find({city: ctx.params.city, toUserId: Number(ctx.params.userId)}, {city: 0}).toArray();
    docs = docs.map((doc) => {
       doc['likeCount'] = doc.likes.length;
       return doc;
    });
    ctx.body = {
        'data': {
            'comments': docs
        }
    };
    ctx.type = 'application/json; charset=utf-8';
});

// Получить все комменатрии в комнате
router.get('/:city/comments', async (ctx) => {
    const commentsRepo = ctx.mongoDb.collection('comments');
    let docs = await commentsRepo.find({city: ctx.params.city}, {city: 0}).toArray();

    docs = docs.map((doc) => {
        doc['likeCount'] = doc.likes.length;
        return doc;
    });

    ctx.body = {
        'data': {
            'comments': docs
        }
    };
    ctx.type = 'application/json; charset=utf-8';
});

// Добавить пользователя в группу
router.post('/:city/users', async (ctx) => {
    const request = ctx.request.body;
    const userId = Number(request.userId);

    if (!userId) {
        ctx.throw(400, 'Bad param userId');
    }

    const usersRepo = ctx.mongoDb.collection('users');
    try {
        await usersRepo.insertOne({
            city: ctx.params.city,
            userId: userId
        });
    } catch (error) {
        if (error instanceof mongodb.MongoError && error.code === 11000) {
            // no throw
        } else {
            throw error;
        }
    }

    ctx.type = 'application/json; charset=utf-8';
    ctx.status = 201;
});

// Добавить лайк на пожелание
router.post('/comments/:commentId/like', async (ctx) => {
    const request = ctx.request.body;
    const userId = Number(request.userId);

    if (!userId) {
        ctx.throw(400, 'Bad param userId');
    }

    const commentsRepo = ctx.mongoDb.collection('comments');
    const comment = await commentsRepo.findOne({ _id: mongodb.ObjectId(ctx.params.commentId) });
    if (comment === null) {
        ctx.throw(400, 'Bad commentId');
    }

    const usersRepo = ctx.mongoDb.collection('users');
    const countUser = await usersRepo.count({userId: userId, city: comment.city});
    if (countUser !== 1) {
        ctx.throw(400, 'Bad userId');
    }

    await commentsRepo.updateOne(
        {_id: mongodb.ObjectId(ctx.params.commentId)},
        { $addToSet: {likes: userId} }
    );

    ctx.type = 'application/json; charset=utf-8';
    ctx.status = 201;
});

// Удалить лайк с пожелания пользователя
router.del('/comments/:commentId/like', async (ctx) => {
    const request = ctx.request.body;
    if (!request.userId) {
        ctx.throw(400, 'Bad param userId');
    }

    const commentsRepo = ctx.mongoDb.collection('comments');
    await commentsRepo.updateOne(
        {_id: mongodb.ObjectId(ctx.params.commentId)},
        { $pull: {likes: Number(request.userId) } }
    );

    ctx.type = 'application/json; charset=utf-8';
    ctx.status = 200;
});

router.get('/', async (ctx) => {
    ctx.body = 'hello!';
});

module.exports = router;