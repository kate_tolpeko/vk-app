const MONGODB_HOST = process.env.MONGODB_HOST || '127.0.0.1';
const MONGODB_PORT = process.env.MONGODB_PORT || 27019;
const MONGODB_PASS = process.env.MONGODB_PASS || '';
const MONGODB_USER = process.env.MONGODB_USER || 'root';
const MONGO_DB = process.env.MONGODB_DB || 'anderson_vk';
const MONGO_URL = 'mongodb://' + MONGODB_USER + ':' + MONGODB_PASS +'@' + MONGODB_HOST + ':' + MONGODB_PORT + '/' + MONGO_DB;

const SERVER_PORT = process.env.SERVER_PORT || 3000;

module.exports = {
    db: {
        mongo: {
            main: MONGO_URL
        }
    },

    server: {
        port: SERVER_PORT
    }
};