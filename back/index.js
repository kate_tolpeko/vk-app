(async () => {
    const Koa = require('koa');
    const app = new Koa();
    const koaBody = require('koa-body');
    const router = require('./routing.js');
    const cors = require('kcors');
    const MongoClient = require('mongodb').MongoClient;
    const errorHandler = require('./middleware/ErrorHandler.js');
    const config = require('./config.js');

    app.context.mongoDb = await MongoClient.connect(config.db.mongo.main, {
        authSource: 'admin'
    });

    const fs = require('fs');
    const morgan = require('koa-morgan');
    const accessLogFormat = ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" :response-time ms';
    const accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'});

    app
        .use(morgan(accessLogFormat, {stream: accessLogStream}))
        .use(async (ctx, next) => {
            await next();
            if (!ctx.body) {
                ctx.body = {};
            }
            ctx.body.status = ctx.status || 500;
        })
        .use(cors())
        .use(errorHandler)
        .use(koaBody({strict: false}))
        .use(router.routes())
        .use(router.allowedMethods());

    app.listen(config.server.port);
})();