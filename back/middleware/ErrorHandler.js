"use strict";
const debug = require('debug')('error:handler');
const MongoError = require('mongodb').MongoError;

module.exports = async(ctx, next) => {
    debug('middleware');

    try {
        await next();
    } catch (error) {
        let title = 'Error';
        ctx.status = error.status || 500;

        if (error instanceof MongoError && error.code === 11000) {
            title = 'Duplicate model';
        } else {
            title = ctx.status < 500 ? error.message : 'Error';
        }

        ctx.body = {
            error: {
                title
            }
        };
    }
};