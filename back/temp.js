"use strict";
const fetch = require('node-fetch');

(async () => {
    const response = await fetch('https://yandex.ru');
    const text = await response.text();

    console.dir(text, {color: true});
})();
